unit untPrincipal;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.Imaging.pngimage, Vcl.ExtCtrls,
  Vcl.DBCGrids, Vcl.StdCtrls, Vcl.Buttons, System.ImageList, Vcl.ImgList,
  FireDAC.Stan.Intf, FireDAC.Stan.Option, FireDAC.Stan.Param,
  FireDAC.Stan.Error, FireDAC.DatS, FireDAC.Phys.Intf, FireDAC.DApt.Intf,
  Data.DB, FireDAC.Comp.DataSet, FireDAC.Comp.Client;

type
  TuPrincipal = class(TForm)
    Panel3: TPanel;
    Panel4: TPanel;
    Panel5: TPanel;
    Panel2: TPanel;
    Panel1: TPanel;
    SpeedButton1: TSpeedButton;
    SpeedButton2: TSpeedButton;
    SpeedButton4: TSpeedButton;
    SpeedButton5: TSpeedButton;
    Label1: TLabel;
    Panel6: TPanel;
    Shape1: TShape;
    Label2: TLabel;
    Label3: TLabel;
    Label4: TLabel;
    Panel7: TPanel;
    SpeedButton6: TSpeedButton;
    SpeedButton7: TSpeedButton;
    SpeedButton8: TSpeedButton;
    SpeedButton9: TSpeedButton;
    Label5: TLabel;
    SpeedButton10: TSpeedButton;
    DBCtrlGrid1: TDBCtrlGrid;
    Panel8: TPanel;
    Shape2: TShape;
    Shape3: TShape;
    Label6: TLabel;
    Label7: TLabel;
    Shape5: TShape;
    Label8: TLabel;
    Shape6: TShape;
    Label9: TLabel;
    DataSource1: TDataSource;
    FDMemTable1: TFDMemTable;
    FDMemTable1DESCRICAO: TStringField;
    FDMemTable1STATUS: TIntegerField;
    Label11: TLabel;
    FDMemTable1TOTAL: TBCDField;
    FDMemTable1DATAHORA: TDateTimeField;
    SpeedButton3: TSpeedButton;
    SpeedButton11: TSpeedButton;
    procedure SpeedButton3Click(Sender: TObject);
    procedure FormCreate(Sender: TObject);
    procedure DBCtrlGrid1PaintPanel(DBCtrlGrid: TDBCtrlGrid; Index: Integer);
    procedure SpeedButton11Click(Sender: TObject);
  private
    { Private declarations }
  public
    procedure CarregarMesas;
    procedure FiltrarTag(Atag: Integer);
  end;

var
  uPrincipal: TuPrincipal;

implementation

{$R *.dfm}

procedure TuPrincipal.CarregarMesas;
var
  i: Integer;

begin
  FDMemTable1.DisableControls;
  FDMemTable1.Close;
  FDMemTable1.CreateDataSet;
  FDMemTable1.Open;

  for i := 1 to 16 do
  begin
    FDMemTable1.Append;
    FDMemTable1DESCRICAO.AsString := 'Mesa '+ FormatFloat('00', i);
    FDMemTable1STATUS.AsInteger := Random(4);
    FDMemTable1TOTAL.AsFloat := 1 * Random(9);
    FDMemTable1DATAHORA.AsDateTime := now;
    FDMemTable1.Post;
  end;
    FDMemTable1.EnableControls;
end;

procedure TuPrincipal.DBCtrlGrid1PaintPanel(DBCtrlGrid: TDBCtrlGrid; Index: Integer);
begin
  // Label10.Caption := 'Iniciar';
  Label6.Caption := FDMemTable1DESCRICAO.AsString;
  Label11.Caption := FormatFloat('R$ ###,###,#0.00', FDMemTable1TOTAL.AsFloat);
  Label8.Caption := 'Aberto: h� ' + FormatDateTime('hh', Now - FDMemTable1DATAHORA.AsDateTime);

  case FDMemTable1STATUS.AsInteger of
    0:
    begin
      Shape6.Brush.Color := clRed;
      Shape3.Brush.Color := clRed;
      Label9.Caption := 'Ocupada';
      // Label10.Caption := 'Resumo';
    end;
    1:
    begin
      Shape6.Brush.Color := clGreen;
      Shape3.Brush.Color := clGreen;
      Label9.Caption := 'Livre';
    end;
    2:
    begin
      Shape6.Brush.Color := $000080FF;
      Shape3.Brush.Color := $000080FF;
      Label9.Caption := 'Reservada';
    end;
    3:
    begin
      Shape6.Brush.Color := clGray;
      Shape3.Brush.Color := clGray;
      Label9.Caption := 'Finalizada';
    end;
  end;
end;

procedure TuPrincipal.FiltrarTag(Atag: Integer);
begin
  if (Atag > -1) then
  begin
    FDMemTable1.Filtered := False;
    FDMemTable1.Filter := 'Status = '+ Atag.ToString;
    FDMemTable1.Filtered := True;
  end
  else
  begin
    FDMemTable1.Filtered := False;
    FDMemTable1.Filter := '';
    FDMemTable1.Filtered := True;
  end;
end;

procedure TuPrincipal.FormCreate(Sender: TObject);
begin
  CarregarMesas;
end;

procedure TuPrincipal.SpeedButton11Click(Sender: TObject);
begin
   FiltrarTag(TSpeedButton(Sender).Tag);
end;

procedure TuPrincipal.SpeedButton3Click(Sender: TObject);
begin
  CarregarMesas;
end;

end.
